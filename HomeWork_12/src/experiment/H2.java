package experiment;

import java.sql.*;

public class H2 {
    static String url = "jdbc:mysql://localhost:3306/studb";
    static String dbName = "root";
    static String password ="wjl2005516";
    static Connection connection = null;
    static PreparedStatement psmt = null;
    static ResultSet rs = null;

    public static void main(String[] args) {
        try {
            //调取方法获取连接
            getconn();
            //sql语句
            String sql = "select * from examination";
            //设置参数

            //预编译
            psmt = connection.prepareStatement(sql);

            //结果
            rs = psmt.executeQuery();
            while (rs.next()){
                String id = rs.getNString("id");
                String name = rs.getString("name");
                double javaP = rs.getDouble("javaP");
                double math = rs.getDouble("math");
                System.out.println(id+"\t"+name+"\t"+javaP+"\t"+math);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public static void  getconn(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(url,dbName,password);

        } catch (Exception e) {
                e.printStackTrace();

        }
    }
    public static void close(){
        try {
            connection.close();
            rs.close();
            psmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
