package experiment;

import java.sql.*;
import java.util.Scanner;

public class H3 {
    public static void main(String[] args) throws Exception {

        //获取输入的数据
        System.out.println("新插入一条记录，请输入学号，姓名和成绩:");
        Scanner scanner = new Scanner(System.in);
        String id = scanner.next();
        String name = scanner.next();
        double score = scanner.nextDouble();


        //加载数据库驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //获取数据库连接
        String url = "jdbc:mysql://localhost:3306/studb";
        String username = "root";
        String password = "wjl2005516";
        Connection conn = DriverManager.getConnection(url, username, password);

        //sql语句
        String sql = "insert into course(id, name, score) value (?,?,?)";

        //获取psmt对象
        PreparedStatement psmt = conn.prepareStatement(sql);

        //设置参数
        psmt.setString(1, id);
        psmt.setString(2, name);
        psmt.setDouble(3, score);

        //执行sql
        int count = psmt.executeUpdate();

        //sql语句
        String SQL = "select * from course";

        PreparedStatement preparedStatement = conn.prepareStatement(SQL);

        ResultSet rs = preparedStatement.executeQuery();
        if(count>0){
            while (rs.next()){
                //获取数据的数据
                String ID =rs.getNString("id");
                String NAME = rs.getNString("name");
                double SCORE = rs.getDouble("score");

                System.out.println(ID+"\t"+NAME+"\t"+SCORE);
            }
        }
        //释放资源
        psmt.close();
        conn.close();


    }
}
