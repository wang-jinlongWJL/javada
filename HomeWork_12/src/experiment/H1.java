package experiment;

import java.sql.*;
public class H1 {
    public static void main(String[] args) throws Exception {
        //加载数据库驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //获取连接
        String url = "jdbc:mysql://localhost:3306/studb";
        String username = "root";
        String password = "wjl2005516";
        Connection connection = DriverManager.getConnection(url,username,password);

        //sql语句
        String sql = "select * from course";

        //获取psmt对象
        PreparedStatement psmt = connection.prepareStatement(sql);

        //获取参数

        //执行sql
        ResultSet rs = psmt.executeQuery();
        while (rs.next()){
            String id = rs.getNString("id");
            String name = rs.getNString("name");
            double score = rs.getDouble("score");
            System.out.println(id+"\t"+name+"\t"+score);
        }
        //关闭资源
        connection.close();
        psmt.close();
        rs.close();
    }
}
