package op_ed_meth0d_override01;

public abstract class Person {
    int jobNumber;
    String name;
    int wages;

    //构造空参
    public Person() {
    }
    //构造全参

    public Person(int jobNumber, String name, int wages) {
        this.jobNumber = jobNumber;
        this.name = name;
        this.wages = wages;
    }

    public int getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(int jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWages() {
        return wages;
    }

    public void setWages(int wages) {
        this.wages = wages;
    }

    //打印信息
    public void show(){
        System.out.println(this.getJobNumber()+", "+this.getName()+", "+this.getWages());
    }
    //吃饭
    public void eat(){
        System.out.println(this.name+"吃米饭");
    }

    //工作
    public abstract void work();
}
