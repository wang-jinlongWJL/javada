package test1;

public class Student {
    private String name;
    private int age;
    private String gendar;

    //构造空参

    public Student() {
    }
    //构造全参

    public Student(String name, int age, String gendar) {
        this.name = name;
        this.age = age;
        this.gendar = gendar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGendar() {
        return gendar;
    }

    public void setGendar(String gendar) {
        this.gendar = gendar;
    }
}
