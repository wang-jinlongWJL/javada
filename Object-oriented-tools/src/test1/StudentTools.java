package test1;

public class StudentTools {
    private StudentTools(){}

    //求三名学生中最大年龄
    public static int getMaxAge(Student[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            Student stu = new Student();
            stu = arr[i];
            if(stu.getAge()>max){
                max = stu.getAge();
            }
        }
        return max;
    }

}
