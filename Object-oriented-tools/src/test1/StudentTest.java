package test1;

public class StudentTest {
    public static void main(String[] args){
        Student stu1 = new Student("张三",18,"男");
        Student stu2 = new Student("张四",19,"男");
        Student stu3 = new Student("张无",19,"男");
        Student[] arr = new Student[3];
        arr[0] = stu1;
        arr[1] = stu2;
        arr[2] = stu3;
        //调用求最大年龄
        int MaxAge = StudentTools.getMaxAge(arr);
        for (int i = 0; i < arr.length; i++) {
            Student stu = new Student();
            stu = arr[i];
            if(MaxAge == stu.getAge()){
                System.out.println("最大年龄为："+MaxAge+"学生姓名为：" + stu.getName());
            }
        }
        //System.out.println(MaxAge);
    }
}
