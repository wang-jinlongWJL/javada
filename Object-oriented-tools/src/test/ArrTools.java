package test;

public class ArrTools {
    private ArrTools(){}

    //打印数组 以这样的形式 [a,c,d,f,g]
    public static String printArr(int[] arr){
        StringBuilder sb = new StringBuilder();
        //最前面加上[
        sb.append("[");
        for (int i = 0; i <arr.length; i++) {
            if(i == arr.length-1){
                sb.append(arr[i]);
            }
            else {
                sb.append(arr[i]).append(", ");
            }
        }
        //最后补上]
        sb.append("]");
        //返回拼接完成的字符串
        return sb.toString();
    }

    //求浮点数数组的平均值
    public static double gerAverage(double[] arr){
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum/arr.length;
    }
}
