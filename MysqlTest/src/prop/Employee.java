package prop;

import java.util.Date;

public class Employee {
    private int id;
    private int WorkId;
    private String name;
    private String gender;
    private int age;
    private String IdCard;
    private String WorkAddress;
    private String EntryDate;
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorkId() {
        return WorkId;
    }

    public void setWorkId(int workId) {
        WorkId = workId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getIdCard() {
        return IdCard;
    }

    public void setIdCard(String idCard) {
        IdCard = idCard;
    }

    public String getWorkAddress() {
        return WorkAddress;
    }

    public void setWorkAddress(String workAddress) {
        WorkAddress = workAddress;
    }

    public String getEntryDate() {
        return EntryDate;
    }

    public void setEntryDate(String entryDate) {
        EntryDate = entryDate;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", WorkId=" + WorkId +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", IdCard='" + IdCard + '\'' +
                ", WorkAddress='" + WorkAddress + '\'' +
                ", EntryDate=" + EntryDate +
                ", status=" + status +
                '}';
    }
}
