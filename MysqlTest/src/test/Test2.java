package test;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;

public class Test2 {

    public static void main(String[] args) throws Exception {
        if(testDelete())
        {
            System.out.println("删除成功");
        }
        else {
            System.out.println("删除失败");
        }
    }
    public static boolean testDelete() throws Exception {
        //1.连接connection

        //3.加载配置文件
        Properties prop = new Properties();
        DruidDataSourceFactory.createDataSource(prop);
        prop.load(new FileInputStream("src/druid.properties"));
        //4. 获取连接池对象
        DataSource dataSource = DruidDataSourceFactory.createDataSource(prop);

        //5. 获取数据库连接connection

        Connection connection = dataSource.getConnection();

        //2. sql语句
        String sql = "delete from employee where id = ?";

        //3. 获取psmt对象
        PreparedStatement psmt = connection.prepareStatement(sql);

        //4.设置参数
        psmt.setInt(1,1);

        //5.执行sql
        int count = psmt.executeUpdate();//影响的行数

        //6.处理结果

        if(count>0)
        {
            //7 释放资源
            connection.close();
            psmt.close();
            return true;
        }
        else {
            //7 释放资源
            connection.close();
            psmt.close();
            return false;
        }
    }
}
