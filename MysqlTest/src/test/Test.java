package test;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import prop.Employee;

import javax.sql.DataSource;
import javax.xml.transform.Result;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/*

这个页面用来进行用户的增删改查

 */
public class Test {


    public static void main(String[] args) throws Exception {
        selectAll();
    }
    //查询所有
    //1 sql语句 select * form employee
    //2 参数不需要
    //3 返回结果 list<employee>

    public static void selectAll() throws Exception {

        //1. 获取 connection
        //3.加载配置文件
        Properties prop = new Properties();
        DruidDataSourceFactory.createDataSource(prop);
        prop.load(new FileInputStream("src/druid.properties"));
        //4. 获取连接池对象
        DataSource dataSource = DruidDataSourceFactory.createDataSource(prop);

        //5. 获取数据库连接connection

        Connection connection = dataSource.getConnection();

        //2.sql语句
        String sql = "select * from employee";

        //3.获取psmt对象
        PreparedStatement psmt = connection.prepareStatement(sql);

        //4.设置参数z

        //5.执行sql
        ResultSet resultSet = psmt.executeQuery();

        //6.处理结果 list<employee> 封装employee 装载集合
        Employee employee = null;
        List<Employee> employees = new ArrayList<>();
        while (resultSet.next()){
            //获取数据
            int id = resultSet.getInt("id");
            int WorkId = resultSet.getInt("work_id");
            String name = resultSet.getString("name");
            String gender = resultSet.getString("gender");
            int age = resultSet.getInt("age");
            String IdCard = resultSet.getString("id_card");
            String WorkAddress = resultSet.getString("work_address");
            String EntrtDate = resultSet.getString("entry_date");
            //封装employee
            employee = new Employee();
            employee.setId(id);
            employee.setWorkId(WorkId);
            employee.setName(name);
            employee.setGender(gender);
            employee.setAge(age);
            employee.setIdCard(IdCard);
            employee.setWorkAddress(WorkAddress);
            employee.setEntryDate(EntrtDate);
            //装载集合
            employees.add(employee);
        }

        System.out.println(employees);
        //7.释放资源
        resultSet.close();
        psmt.close();
        connection.close();


    }

}
