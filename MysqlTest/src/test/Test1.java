package test;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Test1 {
    public static void main(String[] args) throws Exception {
        boolean flag = testAdd();
        if (flag){
            System.out.println("添加成功");
        }
        else {
            System.out.println("添加失败");
        }
    }
    public static boolean testAdd() throws Exception {


        //需要添加的数据
        int id = 17;
        int WorkId = 17;
        String name = "张领想";
        String gender = "男";
        int age = 17;
        String IdCard = "2443874283482234";
        String WorkAddress = "北京";
        String EntryDate = "2000-01-01";


        //1. 获取 connection
        //3.加载配置文件
        Properties prop = new Properties();
        DruidDataSourceFactory.createDataSource(prop);
        prop.load(new FileInputStream("src/druid.properties"));
        //4. 获取连接池对象
        DataSource dataSource = DruidDataSourceFactory.createDataSource(prop);

        //5. 获取数据库连接connection

        Connection connection = dataSource.getConnection();

        //2 sql语句
        String sql = "INSERT INTO employee (id, work_id, name, gender, age, id_card, work_address, entry_date) VALUES(?,?,?,?,?,?,?,?)";

        //3. 获取psmt对象
        PreparedStatement psmt = connection.prepareStatement(sql);

        //4.设置参数
        psmt.setInt(1,id);
        psmt.setInt(2,WorkId);
        psmt.setString(3,name);
        psmt.setString(4,gender);
        psmt.setInt(5,age);
        psmt.setString(6,IdCard);
        psmt.setString(7,WorkAddress);
        psmt.setString(8, EntryDate);

        //5. 执行sql
       int count = psmt.executeUpdate();//影响的行数

        //6.处理结果
        if (count>0){
            //7.释放资源
            psmt.close();
            connection.close();
            return true;
        }
        else {
            //7.释放资源
            psmt.close();
            connection.close();
            return false;
        }
    }
}
