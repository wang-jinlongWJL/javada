package HA;
public  class  ShapeTest  {

    public  static  void  main(String[]  args)  {

        Rectangle  r  =  new  Rectangle();
        System.out.println(r.area()+","+r.perimeter());
        Circle  c  =  new  Circle();
        System.out.println(c.area()+","+c.perimeter());

        Shapes  s  =  r;
        s  =  c;

        r  =  new  Rectangle(1,2);
        System.out.println(r.area()+","+r.perimeter());

        c  =  new  Circle(3);
        System.out.println(c.area()+","+c.perimeter());

    }

}

class  Shapes{
    double  area()  {
        return  0;
    }

    double  perimeter()  {
        return  0;
    }
}
//Rectangle    class
class Rectangle extends Shapes{
    double x = 1.0;
    double y = 1.0;

    public Rectangle(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Rectangle() {
    }

    @Override
    double  area()  {
        return  x*y;
    }
    @Override
    double  perimeter()  {
        return  2*(x+y);
    }
}

//Circle  class
class Circle extends Shapes{
    double r = 1;

    public Circle(double r) {
        this.r = r;
    }

    public Circle() {
    }
    @Override
    double  area()  {
        return  Math.PI*r*r;
    }
    @Override
    double  perimeter()  {
        return  2*Math.PI*r;
    }
}
