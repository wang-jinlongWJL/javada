package HE;

abstract   class  Water  {

    public abstract void drink();
    public abstract void tostring();
}

//class  Nongfu  and  class  Wahaha
class Nongfu extends Water{

    @Override
    public void drink() {
        System.out.print("I like Nongfu--");
    }

    @Override
    public void tostring() {
        System.out.println("My water is:Nongfu");
    }
}

class Wahaha extends Water{

    @Override
    public void drink() {
        System.out.print("I like Wahaha--");
    }

    @Override
    public void tostring() {
        System.out.println("My water is:Wahaha");
    }
}

public  class  WaterTest  {
    public  static  void  main(String[]  args)  {

        Water[]  wine  =  new  Water[2];
        Wahaha wa = new Wahaha();
        Nongfu nf = new Nongfu();
        wine [0] = wa;
        wine [1] = nf;
        wine[0].drink();
        wine[0].tostring();
        wine[1].drink();
        wine[1].tostring();
    }




}
