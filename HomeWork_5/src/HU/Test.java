package HU;

public class Test {
    public static void main(String[] args) {
        JNC j = new JNC();
        j.drink();
        j.toString1();
        WLY w = new WLY();
        w.drink();
        w.toString1();
    }
}
abstract class Wine{
    public abstract void drink();
    public abstract void toString1();
}
class JNC extends Wine{

    @Override
    public void drink() {
        System.out.print("Wine:");
    }

    @Override
    public void toString1() {
        System.out.println("JNC--My wine is:JNC");
    }
}
class WLY extends Wine{

    @Override
    public void drink() {
        System.out.print("Wine:");
    }

    @Override
    public void toString1() {
        System.out.println("WLY--My  wine  is:WLY");
    }
}