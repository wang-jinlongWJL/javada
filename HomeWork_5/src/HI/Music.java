package HI;

public  class  Music  {

    public  static  void  main(String[]  args)  {
        //  TODO  Auto-generated  method  stub
        Instrument[]  orchestra=  {
                new  Wind(),
                new  Percussion(),
                new  Stringed(),
                new  Brass(),
                new  WoodWind()
        };
        tuneAll(orchestra);
    }

    static  void  tune(Instrument  i)  {
        i.play("Savage  gradon");
    }

    static  void  tuneAll(Instrument[]  e)  {
        for(Instrument  i:e)  {
            tune(i);
        }
    }
}

abstract  class  Instrument{
    public abstract void play(String s);
    public abstract void adjust();
    public abstract String what();

}

class  Wind  extends  Instrument{


    @Override
    public void play(String s) {
        System.out.println("Wind"+s);
    }

    @Override
    public void adjust() {

    }

    @Override
    public String what() {
        return null;
    }
}

class  Percussion  extends  Instrument{


    @Override
    public void play(String s) {
        System.out.println("Percussion"+s);
    }

    @Override
    public void adjust() {

    }

    @Override
    public String what() {
        return null;
    }
}

class  Stringed  extends  Instrument{


    @Override
    public void play(String s) {
        System.out.println("Stringed"+s);
    }

    @Override
    public void adjust() {

    }

    @Override
    public String what() {
        return null;
    }
}

class  Brass  extends  Wind{
    @Override
    public void play(String s) {
        System.out.println("Brass"+s);
    }

}

class  WoodWind  extends  Wind{
@Override
public void play(String s) {
    System.out.println("WoodWind"+s);
}

}