package HW;

public  class  JiChengTtest1  {

    public  static  void  main(String[]  args)  {
        //  TODO  Auto-generated  method  stub
        String  name  =  "a";
        String  id  =  "1";
        String  name1  =  "b";
        String  id1  =  "2";
        Animal  a1  =  new  Hamster(name,Integer.parseInt(id));
        Animal  a2  =  new  Mouse(name1,Integer.parseInt(id1));

        eat(a1);
        eat(a2);
    }

    public    static    void    eat(Animal    obj)    {
        System.out.println("call  eat(Animal    obj)  function");
        //在这里调用
        obj.eat();
        if(obj instanceof Mouse)
        {
            eat((Mouse)obj);
        }
                else  {
            System.out.println("is  not  a  Mouse  obj");
        }
                if(obj instanceof Hamster)
        {
            eat((Hamster)obj);
        }
                else  {
            System.out.println("is  not  a  Hamster  obj");
        }

    }
    public    static    void    eat(Mouse    obj)    {
        System.out.println("call  eat(Mouse    obj)  function");

    }

    public    static    void    eat(Hamster    obj)    {
        System.out.println("call  eat(Hamster    obj)  function");
    }

}


class  Animal  {
    protected  String  name;
    protected  int  id;
    public  Animal(String  myName,  int  myid)  {
        name  =  myName;
        id  =  myid;
    }
    public  void  eat(){
        System.out.println(name+"  eating,Animal");
    }

}

class Mouse extends Animal{

    public Mouse(String myName, int myid) {
        super(myName, myid);
    }
    @Override
    public  void  eat(){
        System.out.println(name+"  eating,Mouse id:"+id);
    }
}
class Hamster extends Animal{

    public Hamster(String myName, int myid) {
        super(myName, myid);
    }
    @Override
    public  void  eat(){
        System.out.println(name+"  eating,Hamster id:"+id);
    }
}

