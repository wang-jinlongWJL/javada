package HQ;

public    class    JiChengTtest    {

    public    static    void    main(String[]    args)    {
        //    TODO    Auto-generated    method    stub
        String    name    =    "a";
        String    id    =    "1";
        String    name1    =    "b";
        String    id1    =    "2";
        //编译看左边，运行看右边
        //需要实现多态要确保,子类中存在父类方法的重写
        Penguin a1    =    new    Penguin(name,Integer.parseInt(id));//Integer.parseInt(id)作用是将一个字符串表示的整数转换为对应的int类型整数
        Mouse a2    =    new    Mouse(name1,Integer.parseInt(id1));

        a1.eat();
        a1.sleep();
        a1.introduction();
        eat(new    Penguin(name,Integer.parseInt(id)));

        a2.eat();
        a2.sleep();
        a2.introduction();
        sleep(new    Mouse(name1,Integer.parseInt(id1)));

    }
    public    static    void    eat(Animal    obj)    {
        obj.eat();
    }
    public    static    void    sleep(Animal    obj)    {
        obj.sleep();
    }
}

class  Animal  {
    String  name;
    int  id;
    public  Animal(String  myName,  int  myid)  {
        name  =  myName;
        id  =  myid;
    }
    public  void  eat(){
        System.out.println(name+"  eating");
    }
    public  void  sleep(){
        System.out.println(name+"  sleeping");
    }
    public  void  introduction()  {
        System.out.println("Hello,my  id  is  "  +  id  +  ",name  is  "  +  name  +  ".");
    }
}



class Penguin extends Animal {
    public Penguin(String myName, int myid) {
        super(myName, myid);
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void introduction() {
        super.introduction();
    }
}
class Mouse extends Animal {
    public Mouse(String myName, int myid) {
        super(myName, myid);
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void introduction() {
        super.introduction();
    }

}
