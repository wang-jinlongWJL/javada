package HO;

public  class  Findareas{
    public  static  void  main  (String  []agrs){
        Figure  f=  new  Figure(10  ,  10  );
        Rectangle  r=  new  Rectangle(5    , 9 );
        Figure  figref;
        figref=f;
        System.out.println("Area  is  :"+figref.area());
        figref=r;
        System.out.println("Area  is  :"+figref.area());
    }
}

//  class  Figure
class Figure{
    double x;
    double y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Figure() {
    }
    public double area(){
        System.out.println("Inside area for figure.");
        return x*y;
    }
}
class Rectangle extends Figure{
    public Rectangle(double x, double y) {
        super(x, y);
    }

    public Rectangle() {
    }
    public double area() {
        System.out.println("Inside area for rectangle.");
        return super.area();
    }
}
