package test2;

import java.util.Random;

public class Role {
    private String name;
    private int blood;
    private char gendar;
    private String face;

    String[] boyFace = {"凶狠","帅气","可爱","丑陋"};
    String[] girlFace ={"甜美","漂亮","丑陋","高冷"};
    //构造无参
    public Role() {
    }

    //构造全参
    public Role(String name, int blood, char gendar) {
        this.name = name;
        this.blood = blood;
        this.gendar = gendar;
        //随机生成一个长相
        setFace(gendar);
    }

    public void Setname(String name){
        this.name = name;
    }
    public String Getname(){
        return name;
    }
    public void Setblood(int blood){
        this.blood = blood;
    }
    public int Getblood(){
        return blood;
    }
    public void setGendar(char gendar){
        this.gendar = gendar;
    }
    public char getGendar(){
        return gendar;
    }
    public void setFace(char gendar){
        if(gendar == '男'){
            //从boyFace随机生成一个
            Random random = new Random();
            int index = random.nextInt(boyFace.length);
            this.face = boyFace[index];
        }
        else if(gendar == '女'){
            //从girlFace随机生成一个
            Random random = new Random();
            int index = random.nextInt(girlFace.length);
            this.face = girlFace[index];
        }
        else {
            this.face = "不男不女";
        }
    }

    public String getFace(){
        return face;
    }

    //攻击
    //定义 test.Role r1 = new test.Role()
    // test.Role r2 = new test.Role()
    public void Attak(Role role){
        //攻击伤害在1-20随机取值
        Random random = new Random();
        int hurt = random.nextInt(20)+1;
        //剩余的血量
        int Restblood = role.Getblood() - hurt;
        //如果剩下的血为负数，则直接为 0 ；
        if(Restblood<=0)
        {
            Restblood = 0;
        }
        //修改剩下的血量
        Setblood(Restblood);
        System.out.println(this.Getname()+"打了"+role.Getname()+"一下，造成了"+ hurt +
                "点伤害,"+role.Getname()+"还剩"+Restblood+"点血");
    }

    //展示形象
    public void showRoleInfo(){
        System.out.println("姓名为"+Getname());
        System.out.println("血量为"+Getblood());
        System.out.println("性别为"+getGendar());
        System.out.println("长相为"+getFace());
    }
}
