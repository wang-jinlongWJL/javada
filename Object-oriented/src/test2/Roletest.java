package test2;

public class Roletest {
    public static void main(String[] args){
        //创造第一个对象
        Role r1 = new Role("小明",100,'男');
        //创造第二个对象
        Role r2 = new Role("小美",100,'女');

        //展示第一个角色相关信息
        r1.showRoleInfo();

        System.out.println("===================");

        //展示第二个角色相关信息
        r2.showRoleInfo();
        //回合制游戏
        while (true){
            //r1开始攻击r2
            r1.Attak(r2);
            if(r2.Getblood()==0){
                System.out.println(r1.Getname()+"K.O"+r2.Getname());
                break;
            }
            //r2开始反击
            r2.Attak(r1);
            if(r1.Getblood() == 0){
                System.out.println(r2.Getname()+"K.O"+r1.Getname());
                break;
            }
        }
    }
}
