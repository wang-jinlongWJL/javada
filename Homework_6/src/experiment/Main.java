package experiment;

import java.util.Scanner;

class Person {
    public void welcome() {
        System.out.println("有人来了……");
    }
}

class Friend extends Person {
    public void welcome() {
        System.out.println("朋友来了有好酒……");
    }
}

class Enemy extends Person {
    public void welcome() {
        System.out.println("若是那财狼来了迎接他的有猎枪……");
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入若干个整数，这些整数能被3整除的将实例化为Person对象，余1的将实例化为Enemy对象，余2的将实例化为Friend");

        //这个字符串数组可以连续输入多个整数并用空格隔开
        String[] input = scanner.nextLine().split(" ");
        //创建Person的数组，实现多态
        Person[] people = new Person[input.length];

        for (int i = 0; i < input.length; i++) {
            //将String类型的转换为整数类型
            int num = Integer.parseInt(input[i]);
            if (num % 3 == 0) {
                people[i] = new Person();
            } else if (num % 3 == 1) {
                people[i] = new Enemy();
            } else if (num % 3 == 2) {
                people[i] = new Friend();
            }
        }
        //这段代码中的for循环是一个增强型for循环，也称为for-each循环。在这里，它用于遍历数组people中的每个元素，
        // 并将每个元素依次赋给变量p，然后执行循环体内的操作。
        for (Person c : people) {
            c.welcome();
            //言简意赅就是把Perso people中的每个元素赋值给 Person c 然后执行一次
        }

        //调用scanner.close()关闭了Scanner对象，释放资源。
        scanner.close();
    }
}

