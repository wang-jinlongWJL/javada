package experiment;

public  class  DifferentPeopleToString  {

    public  static  void  main(String[]  args)  {
        //  TODO  Auto-generated  method  stub
        m(new GraduateStudent());
        m(new Student());
        m(new Person1());
    }

    public  static  void  m(Object  x)  {
        System.out.println(x.toString());
    }

}

class  Person1{

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}

class  Student  extends  Person{

}

class  GraduateStudent  extends  Student{

}