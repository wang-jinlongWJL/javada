public class Switch {
    int status;
    String productID;
    float maxVoltage;

    public Switch(int status, String productID, float maxVoltage) {
        this.status = status;
        this.productID = productID;
        this.maxVoltage = maxVoltage;
    }

    public Switch() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public float getMaxVoltage() {
        return maxVoltage;
    }

    public void setMaxVoltage(float maxVoltage) {
        this.maxVoltage = maxVoltage;
    }

}
