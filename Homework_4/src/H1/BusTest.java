package H1;

class  Auto  {
    double speed;
    //启动方法
    public void start(){
        System.out.println("The auto is started");
    }
//加速
    public void speedUp(double speed){
        System.out.println("The auto is speed up to "+speed+"kilo/h");
    }
//停止
    public void stop(){
        System.out.println("The auto is stopped");
    }

}
class Bus extends Auto{
    int passenger;
    //上车
    public void gotOn(int passenger){
        System.out.println("The person on bus is: "+passenger);
    }
    public void gotOff(int passenger){
        System.out.println("The person on bus is: "+passenger);
    }
}



public  class  BusTest{
    public  static  void  main(String[]  args)  {
        Bus  bus=new  Bus();
        bus.start();
        bus.speedUp(60);
        bus.stop();
        bus.gotOn(10);
        bus.gotOff(5);
    }
}