package H3;

public    class    DogCatBirdTest    {
    public    static    void    main(String[]    args)    {
        Dog    dog=new    Dog(12,5);
        Cat    cat=new    Cat();
        cat.setAge(3);
        cat.setWeight(6);
        Bird    bird=new    Bird(1,2);
        dog.run();
        cat.run();
        cat.jump();
        bird.fly();
    }
}
 class Animal {
    private double weight;
    private int age;

    //空参

     public Animal() {
     }

     //全参

     public Animal(double weight, int age) {
         this.weight = weight;
         this.age = age;
     }

     public double getWeight() {
         return weight;
     }

     public void setWeight(double weight) {
         this.weight = weight;
     }

     public int getAge() {
         return age;
     }

     public void setAge(int age) {
         this.age = age;
     }

     //跑步
     public void run(){
         System.out.println("Animal run...");
     }
     //跳
     public void jump(){
         System.out.println("Animal jump...");
     }

 }
class Dog extends Animal{
    public Dog() {
    }

    public Dog(double weight, int age) {
        super(weight, age);
    }
    @Override
    public void run(){
        System.out.println("Dog run...");
    }
    @Override
    public void jump(){
        System.out.println("Dog jump...");
    }
}
class Cat extends Animal{
    public Cat() {
    }

    public Cat(double weight, int age) {
        super(weight, age);
    }
    @Override
    public void run(){
        System.out.println("Cat run...");
    }
    @Override
    public void jump() {
        System.out.println("Cat jump...");
    }
}
class Bird extends Animal{
    public Bird() {
    }

    public Bird(double weight, int age) {
        super(weight, age);
    }
    @Override
    public void jump() {
        System.out.println("Bird jump...");
    }

    public void fly(){
        System.out.println("Bird fly...");
    }
}