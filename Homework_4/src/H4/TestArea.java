package H4;

import java.util.Scanner;

public class TestArea {
    public static void main(String[] args) {
        SimpleCircle s = new SimpleCircle();
        Scanner sc = new Scanner(System.in);
        double n = sc.nextInt();
        s.printAreas(n);
    }
}

class SimpleCircle{
    private double radius;

    public SimpleCircle(double radius) {
        this.radius = radius;
    }

    public SimpleCircle() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    //打印圆的面积
    public void printAreas(double radius){
        for (int i = 1; i <= radius; i++) {
            double area = Math.PI*i*i;
            System.out.printf("Area is:%.2f\n",area);
        }
    }
}
