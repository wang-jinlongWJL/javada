import java.util.Scanner;

public class Grades {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int grade = sc.nextInt();
        System.out.print("Input a score:");
        switch (grade/10)
        {
            case 10:
            case 9:
                System.out.println("The grade is Very good.");
                break;
            case 8:
                System.out.println("The grade is Good.");
                break;
            case 7:
                System.out.println("The grade is Medium.");
                break;
            case 6:
                System.out.println("The grade is Pass.");
                break;
            case 5:
            case 4:
            case 3:
            case 2:
            case 1:
            case 0:
                System.out.println("The grade is Not Pass.");
                break;
            default:
                if(grade<0 || grade>100)
                {
                    System.out.println("Illegal input, please try again.");
                    break;
                }
        }
    }
}
