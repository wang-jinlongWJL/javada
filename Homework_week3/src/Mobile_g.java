import java.util.Scanner;

public class Mobile_g {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the number of parameters:");
        int parameters = sc.nextInt();
        System.out.println("Please enter a number from 1 to 5 to view the generation of mobile communication technology:");
        int number = sc.nextInt();
        if (parameters == 1)//可以省略这一步
        {
            System.out.print(number+"G Mobile communication technology");
        }
        else
        {
            System.out.println("Please enter the number 10 or 20, 10 represents the Features of a generation of mobile communication technology, and 20 represents the best company of a generation of mobile communication technology:");
            System.out.print(number+"G Mobile communication technology");
            int numbers = sc.nextInt();
            if(numbers==10)
            {

                switch(number)
                {
                    case 1:
                        System.out.println(",Features: Only voice traffic can be transmitted");break;
                    case 2:
                        System.out.println(",Features: Mobile phones can access the Internet");break;
                    case 3:
                        System.out.println(",Features: Can simultaneously transmit   voice and data information");break;
                    case 4:
                        System.out.println(",Features: Can quickly transmit data,   high quality, audio, video and image");break;
                    case 5:
                        System.out.println(",Features: High data rate, low latency, large system capacity");break;
                }
            }
            else
            {
                switch (number)
                {
                    case 1:
                        System.out.println(",Leading company:Motorola");break;
                    case 2:
                        System.out.println(",Leading company:Nokia");break;
                    case 3:
                        System.out.println(",Leading company:Qualcomm");break;
                    case 4:
                        System.out.println(",Leading company:Apple");break;
                    case 5:
                        System.out.println(",Leading company:Huawei");break;
                }
            }
        }
    }
}
