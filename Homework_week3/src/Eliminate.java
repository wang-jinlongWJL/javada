import java.util.Scanner;

public class Eliminate{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter n: ");
        int n = sc.nextInt();

        int[] arr = new int[n];
        boolean[] isDuplicate = new boolean[n];//未初始化的值都是false

        System.out.println("Enter n numbers: ");
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        int distinctCount = 0;
        for (int i = 0; i < n; i++) {
            if (!isDuplicate[i]) {//是假才会打印
                for (int j = i + 1; j < n; j++) {
                    if (arr[i] == arr[j]) {
                        isDuplicate[j] = true;//如果存在重复数则变为真，在if中！isDuplicate则为假
                    }
                }
                distinctCount++;//统计要打印的数
            }
        }

        System.out.println("The number of distinct values is " + distinctCount);
        for (int i = 0; i < n; i++) {
            if (!isDuplicate[i]) {//和上面判断同理
                System.out.print(arr[i] + " ");
            }
        }
    }
}
