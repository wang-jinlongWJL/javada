import java.util.Scanner;

public class Score {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        double[] fraction= new double[n];
        double sum = 0;
        double average = 0;
        double temp = 0;
        for (int i = 0; i < n; i++) {
            fraction[i] = sc.nextDouble();
            sum = sum + fraction[i];
        }
        //找出最大值和最小值
        for (int i = 0; i < fraction.length-1; i++) {
            for (int j = 0; j < fraction.length-1-i; j++) {
                if(fraction[j]>fraction[j+1])
                {
                    temp = fraction[j];
                    fraction[j] = fraction[j+1];
                    fraction[j+1] = temp;
                }
            }
        }
        double max = fraction[n-1];
        double min = fraction[0];
        double sum1 = sum - max - min;
        average = sum1/(n-2)*1.0;
        System.out.printf("average score:%.2f",average);
    }
}
