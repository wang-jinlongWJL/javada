import java.util.Scanner;

public class Cube {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input a int number a:");
        int a = sc.nextInt();
        float b = sc.nextFloat();
        double c = sc.nextDouble();
        cube(a);
        System.out.println("Please input a float number b:");
        cube(b);
        System.out.println("Please input a double number c:");
        cube(c);
    }
    public static void cube(int num){
        System.out.println("invoke int cube()");
        int nums = num*num*num;
        System.out.println("Cube of a is:"+nums);
    }
    public static void cube(double num){
        System.out.println("invoke double cube()");
        double nums = num*num*num;
        System.out.printf("Cube of c is:%.4f",nums);
    }
    public static void cube(float num){
        System.out.println("invoke float cube()");
        float nums = num*num*num;
        System.out.printf("Cube of b is:%.2f\n",nums);
    }
}
