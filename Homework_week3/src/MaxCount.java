import java.util.Scanner;

public class MaxCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int first = sc.nextInt();
        int max = 0;
        int sum = 1;//统计最大出现多少次
        if (first == 0) {
            System.out.println("Only 0 is inputed.");
            return;//程序截止
        }
        else {
            while (first != 0) {
                if (first > max) {
                    max = first;
                    sum = 1;//自己出现的一次
                } else if (first == max) {
                    sum++;
                }
                first = sc.nextInt();
            }
        }
            System.out.println("max is:" + max);
            System.out.println("The count of the max number is:" + sum);
        }
    }
