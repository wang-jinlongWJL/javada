package Homework;

import java.util.Scanner;

public class CountCharacter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //输入一串字符,输入一行得用到nextLine
        String s = sc.nextLine();
        //定义四个整数类型，用来统计分别有多少个
        int letter = 0;
        int space = 0;
        int number = 0;
        int OtherCharcter = 0;

        for (int i = 0; i < s.length(); i++) {
            //读取到字符串的第一个元素
            char ch = s.charAt(i);
            //是不是字符
            if(Character.isLetter(ch)){
                letter++;
            }
            //是不是空格
            else if(Character.isWhitespace(ch)){
                space++;
            }
            //是不是数字
            else if(Character.isDigit(ch)){
                number++;
            }
            //其他
            else {
                OtherCharcter++;
            }
        }
        System.out.println("The number of letter is:"+letter);
        System.out.println("The number of space is:"+space);
        System.out.println("The number of number is:"+number);
        System.out.println("The number of other character is:"+OtherCharcter);
    }
}