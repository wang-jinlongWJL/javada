package Homework;

import  java.util.Scanner;

public  class  Work  {
    public  static  void  main(String[]  args)  {
        Scanner  input  =  new  Scanner(System.in);
        String  str  =  input.nextLine();
        StringBuilder  sb  =  new  StringBuilder("");

        char  current  =  str.charAt(0);
        int  count  =  1;
        //让count从0开始，出现一次增加一次
        count = 0;
        for (int i = 0; i < str.length(); i++) {
            //出现一次增加一次
            if(str.charAt(i) == current){
                count ++;
            }
            //如过不相等了，则就要进行打印
            if(str.charAt(i) != current)
            {
                //如果进来的count = 1，那么就这sb中添加一个字符
                //如果不是1，那么就打印字符和他出现的次数
                if  (count  ==  1)  sb.append(current);
                else  sb.append(current).append(count);
                //这里需要把count改为1，因为在判断不一样时就已经存在了一次，如果不加上后面计算会少一次
                count = 1;
                //这里要将current刷新
                current = str.charAt(i);
            }
        }
        //最后需要再添加一次
        if  (count  ==  1)  sb.append(current);
        else  sb.append(current).append(count);

        //这里将StringBuilder类型转为String类型并打印
        System.out.println(sb.toString());
    }
}
