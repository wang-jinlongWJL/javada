package experiment;

import java.util.Scanner;

public class LicensePlate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //输入一行字符串
        String input = scanner.nextLine();
        //创建一个StringBuilder
        StringBuilder s = new StringBuilder();
        //定义两个整数类型
        int x = 0;
        int y = 0;
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if(Character.isLetter(ch)){
              //x用来统计字母的个数
                x++;
                char ch1 = Character.toUpperCase(ch);
                s.append(ch1);
            }
            if(Character.isDigit(ch)){
                //这个用来统计数字的个数
                y++;
                s.append(ch);
            }
        }
        //判断形成车牌号的条件
        if(x == 2 && y == 3){
            System.out.println(s.toString());
        } else {
            System.out.println("license plate error.");
        }
    }
}
