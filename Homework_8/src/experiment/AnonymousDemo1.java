interface
polygon{
    public void display();
}


public  class  AnonymousDemo1 {

    public  static  void  main(String[]  args)  {
        //  创建的匿名类继承了  Polygon  接口

        polygon  p1  =

                new polygon(){
                    @Override
                    public void display(){
                        System.out.println("In Anonymous Interface");
                    }
                };


        p1.display();
    }

}