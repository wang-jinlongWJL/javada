package experiment;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HashSetTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //输入一行的名字到换行结束
        String input1 = sc.nextLine();
        //创建一个HashSet规则集，将input1中以逗号隔开的分别放入集合中
        HashSet<String> hashSet = new HashSet<>(Arrays.asList(input1.split(",")));
        //在输入一行
        String input2 = sc.nextLine();
        //再创建一个HashSet规则集，将input1中以逗号隔开的分别放入集合中
        HashSet<String> hashSet1 = new HashSet<>(Arrays.asList(input2.split(",")));

        System.out.print("enter names(separated by , ):");
        //集合排序用到sort方法
        List<String> list = new ArrayList<>();//创建list集合接受排序后的结果
        list = sorted(hashSet);
        System.out.println("your enter:"+list);

        System.out.print("enter names(separated by , ):");
        //排序
        List<String> list1 = new ArrayList<>();//创建list集合接受排序后的结果
        list1 = sorted(hashSet1);
        System.out.println("your enter:"+list1);

        // 交
        HashSet<String> intersectionSet = new HashSet<>(hashSet);
        intersectionSet.retainAll(hashSet1);
        System.out.println("and:" + intersectionSet.stream().sorted().collect(Collectors.toList()));

        // 并
        HashSet<String> unionSet = new HashSet<>(hashSet);
        unionSet.addAll(hashSet1);
        System.out.println("or:" + unionSet.stream().sorted().collect(Collectors.toList()));

        //差
        HashSet<String> result = new HashSet<>(hashSet);
        result.retainAll(hashSet1);
        HashSet<String> all = new HashSet<>(hashSet);
        all.addAll(hashSet1);
        all.removeAll(result);
        System.out.println("diff:"+sorted(all));
    }
    //排序集合的方法
    public static List<String> sorted(HashSet<String> s){
        List<String> list = new ArrayList<>(s);
        Collections.sort(list);
        return list;
    }
}
