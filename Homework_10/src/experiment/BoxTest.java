package experiment;

// 泛型Box类
class Box<T> { // T 代表类型参数

    private T value; // 存储对象

    // 添加元素的方法
    public void add(T item) {
        value = item;
    }

    // 获取元素的方法
    public T get() {
        return value;
    }
}

public class BoxTest {

    public static void main(String[] args) {
        // 创建Integer类型的Box
        Box<Integer> integerBox = new Box<>();
        // 创建String类型的Box
        Box<String> stringBox = new Box<>();

        // 将命令行参数转换为Integer和String
        integerBox.add(123); // 注意这里args[0]应该是字符串形式的整数
        stringBox.add("Tom");

        // 输出
        System.out.printf("Integer is :%d\n", integerBox.get());
        System.out.printf("String is :%s\n", stringBox.get());
    }
}
