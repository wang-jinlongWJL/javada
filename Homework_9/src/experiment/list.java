package experiment;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class list {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        String input = sc.nextLine();
        String[] words = input.split(" ");

        Set<String> linkedHashSet = new LinkedHashSet<>();
        for (String word : words) {
            linkedHashSet.add(word);
        }

        System.out.println(linkedHashSet);
    }
}
