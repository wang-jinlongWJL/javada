package experiment;

import java.util.Scanner;

public class xixi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //输入一个字符串
        String s = scanner.nextLine();
        String reverse = reverse(s);
        System.out.println("enter a string:reverse is:"+reverse);
        System.out.println("ispalindrome:"+isPalindrome(s));
    }
    public static String reverse(String s){
        StringBuilder reversed = new StringBuilder(s).reverse();
        return reversed.toString();
    }
    public static boolean isPalindrome(String s){
        String re = reverse(s);
        //比较两个字符串是否相等
        boolean flag = s.equals(re);
        return flag;
    }
}
