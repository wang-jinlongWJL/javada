package experiment;

import java.util.Scanner;

public class haha {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int characterCount = input.length();
        int wordCount = input.split("[\\s\\p{Punct}]+").length;

        System.out.println(characterCount + " characters");
        System.out.println(wordCount + " words");
    }
}
