import java.util.Scanner;
import java.util.Random;

public class GuessNumber {
    public static void main(String[] args) {
        Random random = new Random();
        int n = random.nextInt(100);
        System.out.println("随机数"+n);
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个数");
        while (true) {
            int number = scanner.nextInt();
            if (n == number) {
                System.out.println("恭喜你猜对了");
                break;
            } else if (number < n) {
                System.out.println("猜小了！");
            } else {
                System.out.println("猜大了！");
            }
        }
    }
}