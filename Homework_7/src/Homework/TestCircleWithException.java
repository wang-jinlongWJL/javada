package Homework;

import  java.util.*;
public  class  TestCircleWithException  {
    public  static  void  main(String[]  args)  {
        Scanner  in  =  new  Scanner(System.in);
        int  radius  =  in.nextInt();
        try {
            CircleWithException cwe = new CircleWithException(radius);
            double s = cwe.findArea();
            System.out.printf("The area of the circle is:%.2f",s);
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getClass().getName()+":"+e.getMessage());
        }finally {
            in.close();
        }

    }
}

class  CircleWithException  {
    private  double  radius;

    public  CircleWithException(double  newRadius)  {
        setRadius(newRadius);
    }

    public  double  getRadius()  {
        return  radius;
    }

    public  void  setRadius(double  newRadius)
            throws  IllegalArgumentException  {
        radius = newRadius;
        if(newRadius<0){
            throw new IllegalArgumentException("Radius cannot be negative");
        }
    }

    public  double  findArea()  {
        return  radius  *  radius  *  3.14159;
    }
}
