package Homework;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print("Input a radius:");
            double n = sc.nextDouble();
            double s = computerCircle(n);
            System.out.printf("area=%.2f",s);
        }
        catch (InputMismatchException e){
            System.out.println(e.getClass().getName());
            System.out.println("Number Format Error.");
        }finally {
            sc.close();
        }
    }
    public static double computerCircle(double n){
        return Math.PI*n*n;
    }
}
