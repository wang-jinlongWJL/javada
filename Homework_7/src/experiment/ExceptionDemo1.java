package experiment;

import  java.util.Scanner;

public  class  ExceptionDemo1{
    public  static  void  efun(int  r)  throws  MyException{

        //如果r小于0，则把异常抛给自定义的异常
        if(r<0){
            throw new MyException(r);
        }
    }
    public  static  void  main(String  args[]){
        Scanner  in  =  new  Scanner(System.in);
        int  r  =  in.nextInt();
        //如果没有异常
        try {
            efun(r);
            System.out.println("radius is: "+r);
        } catch (MyException e) {
            System.out.println(e.toString());
        }
        finally {
            System.out.println("Exception is processed");
        }
    }

}

class  MyException  extends  Exception{

    private int errorCode;
    //全参构造
    public MyException(int errorCode){
        this.errorCode = errorCode;
    }
    @Override
    public String toString(){
        return "MyException:" + errorCode;
    }
}