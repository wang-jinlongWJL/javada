package experiment;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        int z = sc.nextInt();
        try {
            //判断能否构成三角形
            JudgeTriangles(x,y,z);
        }catch (Exception e)
        {
            System.out.println("Enter three side lengths:");
            System.out.println("java.lang.IllegalArgumentException: Cannot form a triangle");
        }
    }
    public static void JudgeTriangles(int x, int y, int z){
        if(x + y >z && x + z > y && y + z > x){
            System.out.println("Enter three side lengths:");
            System.out.println("The length of the three sides of a triangle is:" + x + " " + y + " " + z);
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
