package experiment;

class  DivDemo  {
    int  div(int  a,int  b)  throws  ArithmeticException,ArrayIndexOutOfBoundsException  {
        int  []arr  =  new  int  [a];
        System.out.println(arr[10]);
        return  a/b;
    }
}

public  class  ExceptionDemo  {

    public  static  void  main(String[]args)  {
        DivDemo  d  =  new  DivDemo();
        int  type  =  20;
        int  a  =  90;
        int  b  =  0;

        try  {
            if(type  <  0)  {
                //  add  throw  Exception

                throw new Exception("NegativeException");

            }
            int  x  =  d.div(a,b);
            System.out.println("x="+x);
        }
        //  add  two  catch

        catch (ArithmeticException e) {
            System.out.println(e.toString());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.toString());
        }

        //  add  catch

        catch (Exception e)

        {
            System.out.println(e.toString());
        }
        finally  {

            System.out.println("Over");

        }

    }
}