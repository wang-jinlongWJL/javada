package Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MyJframe extends JFrame implements ActionListener {
    //创建一个按钮对象
    JButton jButton = new JButton("呵呵");
    //在创建一个对象
    JButton jButton1 = new JButton("哈哈");
    public MyJframe(){
        //设置界面大小
        setSize(603,680);
        //设置界面标题
        setTitle("哈哈哈哈");
        //设置界面置顶
        setAlwaysOnTop(true);
        //设置界面居中
        setLocationRelativeTo(null);
        //设置关闭模式,关闭一个就结束程序
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //取消默认居中
        setLayout(null);


        //给按钮设置大小
        jButton.setBounds(0,0,100,50);
        //给按钮添加事件,动作监听
        jButton.addActionListener(this);
        //设置大小
        jButton1.setBounds(210,210,100,50);
        //给按钮添加事件，动作监听
        jButton1.addActionListener(this);

        //将按钮添加到整个界面当中
        this.getContentPane().add(jButton);
        this.getContentPane().add(jButton1);
        //让界面显示出来，放在最后
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source =e.getSource();
        if(source == jButton){
            //点击一下就会变大
            jButton.setBounds(0,0,200,200);
        }
        else if(source == jButton1){
            //点击就会随便跳
            Random r = new Random();
            jButton1.setLocation(r.nextInt(500),r.nextInt(500));
        }
    }
}
