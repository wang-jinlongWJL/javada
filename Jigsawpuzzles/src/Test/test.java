package Test;

import java.util.Random;

public class test {
    public static void main(String[] args) {
        //定义一个一维数组
        int[] tempArr = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        //将一维数组打乱
        Random r =new Random();
        int temp = 0;
        for (int i = 0; i < tempArr.length; i++) {
            int index = r.nextInt(tempArr.length);
            temp = tempArr[i];
            tempArr[i] = tempArr[index];
            tempArr[index] = temp;
        }
        //遍历一维数组
        for (int i = 0; i < tempArr.length; i++) {
            System.out.print(tempArr[i] + " ");
        }

        //定义一个二维数组
        int[][] arr = new int[4][4];
        //将一维数组放到二维数组中
        for (int i = 0; i < tempArr.length; i++) {
            arr[i/4][i%4] = tempArr[i];
        }
        //遍历二维数组
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }
    }
}
