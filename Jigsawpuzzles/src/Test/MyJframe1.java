package Test;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyJframe1 extends JFrame implements KeyListener {

    //添加按钮
    JButton jButton = new JButton();
    public MyJframe1(){
        //设置大小
        setSize(500,500);
        //设置标题
        setTitle("呵呵呵");
        //将页面置顶
        setAlwaysOnTop(true);
        //设置页面居中
        setLocationRelativeTo(null);
        //取消默认居中
        setLayout(null);
        //设置关闭方式
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //显示界面
        setVisible(true);

        //设置按钮大小
        jButton.setBounds(0,0,80,50);
        //给按钮添加事件
        jButton.addKeyListener(this);

        //将按钮添加到界面
        this.getContentPane().add(jButton);

    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        //按下不松将会一直执行
        System.out.println("按下不松");
        int index = e.getKeyCode();
        if(index == 65){
            System.out.println("按下了A");
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("松开按键");
    }
}
