package App;

import javax.swing.*;

public class loginJframe extends JFrame {
    public loginJframe(){
        setSize(488,430);//设置界面长宽高
        setTitle("拼图 登录");//设置标题
        setAlwaysOnTop(true);//设置界面置顶
        setLocationRelativeTo(null);//设置界面居中
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//设置程序结束方式，关闭此窗口程序结束
        setVisible(true);
    }
}
