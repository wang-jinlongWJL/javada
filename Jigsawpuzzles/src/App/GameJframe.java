package App;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

public class GameJframe extends JFrame implements KeyListener , ActionListener {

    //创建更换图片
    JMenu changeImage = new JMenu("更换图片");
    //jMenuBar的条目
    JMenuItem replayGame = new JMenuItem("重新游戏");
    JMenuItem replayLogin = new JMenuItem("重新登录");
    JMenuItem closeGame = new JMenuItem("关闭游戏");

    JMenuItem OfficialAccount = new JMenuItem("公众号");
    JMenuItem girl = new JMenuItem("女孩");
    JMenuItem animal = new JMenuItem("动物");
    JMenuItem sport = new JMenuItem("运动");


    //定义一个二维数组
    //目的;便于管理和修改代码
    int[][] arr = new int[4][4];

    //定义x,y用来记录空白格的坐标
    int x = 0;
    int y = 0;

    //定义一个字符串用来保存图片的路径，方便修改
    String path = "D:\\java\\javada\\Jigsawpuzzles\\image\\girl\\girl4\\";

    //定义一个胜利的数组
    int win[][] = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 0}
    };

    //定义一个计步数的值
    int steoCount = 0;

    public GameJframe() {
        //初始化游戏界面

        intiJFranme();
        //初始化菜单

        intiJMenuBar();

        //初始化数据，将拼图进行打乱，放到二维数组中去
        intiDate();

        //初始化图片

        intiMage();

        setVisible(true);//展示,这个应该放在最后面
    }

    private void intiDate() {
        //定义一个一维数组
        int[] tempArr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        //将一维数组打乱
        Random r = new Random();
        int temp = 0;
        for (int i = 0; i < tempArr.length; i++) {
            int index = r.nextInt(tempArr.length);
            temp = tempArr[i];
            tempArr[i] = tempArr[index];
            tempArr[index] = temp;
        }
        //将一维数组放到二维数组中
        for (int i = 0; i < tempArr.length; i++) {
            //如果是空白格则记录坐标
            if (tempArr[i] == 0) {
                x = i / 4;
                y = i % 4;
                //不是空白格存到二维数组
            }
                arr[i / 4][i % 4] = tempArr[i];

        }
        //遍历数组

        /*for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] +" ");
            }
            System.out.println();
        }*/
    }
    //初始化图片

    private void intiMage() {
        //清空原本的图片
        this.getContentPane().removeAll();

        //细节：先加载的图片在上面，后加载的会被覆盖，若要添加背景图片，应添加在后面

        if (victory()) {
            JLabel win = new JLabel(new ImageIcon("D:\\java\\javada\\Jigsawpuzzles\\image\\win.png"));
            win.setBounds(203, 283, 197, 73);
            this.getContentPane().add(win);
        }

        //绝对路径 ：从盘符开始的 D:\
        //相对路径是从项目文件开始的

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                //取二维数组
                int index = arr[i][j];
                //创建一个Jlabe的对象（管理容器）
                JLabel jLabel = new JLabel(new ImageIcon(path + index + ".jpg"));
                //指定图片位置
                jLabel.setBounds(j * 105 + 83, i * 105 + 134, 105, 105);
                //给图片添加边框
                //0（RAISED） ：使图片立体 1 (LOWERED)：使图片凹下去
                jLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
                //将管理容器添加到界面
                getContentPane().add(jLabel);
            }
        }

        //添加背景图片
        ImageIcon background = new ImageIcon("D:\\java\\javada\\Jigsawpuzzles\\image\\background.png");
        JLabel back = new JLabel(background);
        //指定图片位置，设置大小
        back.setBounds(40, 40, 508, 560);
        //最后把这个添加到界面中
        this.getContentPane().add(back);

        //添加计步器
        JLabel stepCount = new JLabel("步数 : " + steoCount);
        stepCount.setBounds(50, 30, 100, 20);
        this.getContentPane().add(stepCount);

        //刷新一下界面
        this.getContentPane().repaint();
    }

    private void intiJFranme() {
        setSize(603, 680);//设置界面的长宽高
        setTitle("拼图单机版");//设置标题
        setAlwaysOnTop(true);//设置界面置顶
        setLocationRelativeTo(null);//设置界面居中
        setLayout(null);//取消居中放置
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//设置程序结束方式，关闭此窗口程序结束
        //给游戏界面添加键盘监听
        this.addKeyListener(this);
    }

    private void intiJMenuBar() {
        //主菜单
        JMenuBar jMenuBar = new JMenuBar();

        JMenu function = new JMenu("功能");
        JMenu aboutMine = new JMenu("关于我们");

        //将这个添加到主菜单下面
        jMenuBar.add(function);
        jMenuBar.add(aboutMine);

        //给条目添加动作监听
        replayGame.addActionListener(this);
        replayLogin.addActionListener(this);
        closeGame.addActionListener(this);
        OfficialAccount.addActionListener(this);

        //给更换图片里的条目添加动作监听
        girl.addActionListener(this);
        animal.addActionListener(this);
        sport.addActionListener(this);

        //讲这些添加到JMenu下面
        function.add(replayGame);
        function.add(replayLogin);
        function.add(closeGame);
        function.add(changeImage);

        aboutMine.add(OfficialAccount);

        //在更换图片中添加条目
        changeImage.add(girl);
        changeImage.add(animal);
        changeImage.add(sport);

        //给整个界面设置菜案单
        setJMenuBar(jMenuBar);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        //判断是否胜利
        if (victory()) {
            return;
        } else {
            int index = e.getKeyCode();
            if (index == 65) {
                JLabel all = new JLabel(new ImageIcon(path + "\\all.jpg"));
                all.setBounds(83, 134, 420, 420);
                //清空图片
                this.getContentPane().removeAll();
                //把完整图片添加到游戏界面
                this.getContentPane().add(all);

                //添加背景图片
                ImageIcon background = new ImageIcon("D:\\java\\javada\\Jigsawpuzzles\\image\\background.png");
                JLabel back = new JLabel(background);
                //指定图片位置，设置大小
                back.setBounds(40, 40, 508, 560);
                //最后把这个添加到界面中
                this.getContentPane().add(back);
                //重新刷新一下
                this.getContentPane().repaint();
                //在松开A时，需要重新加载拼图
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        //判断是否胜利，胜利将结束代码
        if (victory()) {
            return;
            //结束代码
        }
        //如果没胜利将继续执行
        else {

            //左 37 上 38 右 39 下 40
            int index = e.getKeyCode();
            //向上移动
            if (index == 38) {

                System.out.println("向上移动");
                //将下面的图片和空白格互换
                if (x == 3) {
                    //此时在最下面没有图片可以上移
                    return;
                }
                arr[x][y] = arr[x + 1][y];
                arr[x + 1][y] = 0;
                //此时空白格的位置x会加1
                x++;
                //计算步数
                steoCount++;
                //重新初始化图片
                intiMage();
            }
            //向下移动
            else if (index == 40) {
                System.out.println("向下移动");
                //将上面的图片和空白格互换
                if (x == 0) {
                    //图片在最上面，没有图片可以下移
                    return;
                }
                arr[x][y] = arr[x - 1][y];
                arr[x - 1][y] = 0;
                //此时空白格位置会减1
                x--;
                //计算步数
                steoCount++;
                //重新初始化图片
                intiMage();
            }
            //向左移动
            else if (index == 37) {
                System.out.println("向左移动");
                if (y == 3) {
                    //空白在最右边，没有图片左移
                    return;
                }
                //将上面的图片和空白格互换
                arr[x][y] = arr[x][y + 1];
                arr[x][y + 1] = 0;
                //此时空白格位置会减1
                y++;
                //计算步数
                steoCount++;
                //重新初始化图片
                intiMage();
            } else if (index == 39) {
                System.out.println("向右移动");
                if (y == 0) {
                    //图片在最左边，没有图片右移
                    return;
                }
                //将上面的图片和空白格互换
                arr[x][y] = arr[x][y - 1];
                arr[x][y - 1] = 0;
                //此时空白格位置会减1
                y--;
                //计算步数
                steoCount++;
                //重新初始化图片
                intiMage();
            } else if (index == 65) {
                //直调用初始化图片重新加载游戏界面
                intiMage();
            } else if (index == 87) {
                //按下w直接胜利
                arr = new int[][]{
                        {1, 2, 3, 4},
                        {5, 6, 7, 8},
                        {9, 10, 11, 12},
                        {13, 14, 15, 0}
                };
                //更改完数据后，重新加载游戏界面
                intiMage();
            }
        }
    }

    //判断是否胜利
    public boolean victory() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] != win[i][j]) {
                    return false;
                }
            }
        }
        //循环走完了都没有出现不相等，防火true
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == replayGame) {
            System.out.println("重新游戏");
            //计步器清0
            steoCount = 0;
            //重新打乱图片
            intiDate();
            //重新加载图片
            intiMage();

        } else if (obj == replayLogin) {
            System.out.println("重新登陆");
            //关闭游戏界面
            setVisible(false);
            //打开登录界面
            new loginJframe();
        }
        else if(obj == closeGame){
            System.out.println("退出游戏");
            //关闭虚拟机
            System.exit(0);
        }
        else if(obj == OfficialAccount){
            System.out.println("公众号");
            //创建一个弹框对像
            JDialog jDialog = new JDialog();
            //创建一个图片管理器Jlabel
            JLabel jLabel = new JLabel(new ImageIcon("D:\\java\\javada\\Jigsawpuzzles\\image\\about.png"));
            //设置长宽高
            jLabel.setBounds(0,0,258,258);
            //将图片添加到弹框中
            jDialog.getContentPane().add(jLabel);
            //给弹框设置大小
            jDialog.setSize(344,344);
            //让弹框置顶
            jDialog.setAlwaysOnTop(true);
            //让弹框居中
            jDialog.setLocationRelativeTo(null);
            //弹框不关闭无法进行别的操作
            jDialog.setModal(true);
            //让弹框显示
            jDialog.setVisible(true);
        }
        else if(obj == girl){
            //点击女孩从13张女孩中随机选取一张图片
            Random r = new Random();
            int n = r.nextInt(13)+1;
            path = "D:\\java\\javada\\Jigsawpuzzles\\image\\girl\\girl"+n+"\\";
            System.out.println(path);
            //重新加载图片
            intiMage();
        }
        else if(obj == animal){
            //点击动物从13张女孩中随机选取一张图片
            Random r = new Random();
            int n = r.nextInt(8)+1;
            path = "D:\\java\\javada\\Jigsawpuzzles\\image\\animal\\animal"+n+"\\";
            System.out.println(path);
            //重新加载图片
            intiMage();
        }
        else if(obj == sport){
            //点击运动从13张女孩中随机选取一张图片
            Random r = new Random();
            int n = r.nextInt(10)+1;
            path = "D:\\java\\javada\\Jigsawpuzzles\\image\\sport\\sport"+n+"\\";
            System.out.println(path);
            //重新加载图片
            intiMage();
        }
    }
}
