package test4;

import java.util.Scanner;

public class studentTest {
    public static void main(String[] args) {
        student stu1 = new student(001, "zhangsan", 13);
        student stu2 = new student(002, "zhaosi", 12);
        student stu3 = new student(003, "liwu", 14);
        //创建一个数组
        student[] arr = new student[4];
        arr[0] = stu1;
        arr[1] = stu2;
        arr[2] = stu3;


        //添加一个学生对象
        student stu4 = new student(004, "wangliu", 16);
        //判断唯一性
        boolean flag = judgeArrOnly(arr, stu4.getId());
        if (flag) {
            arrStudent(arr, stu4.getId());
        } else {
            System.out.println("重复，请重新输入");
        }

    }
    //添加一个学生对象
    static student stu4 = new student(004,"wangliu",16);

    //添加新学生的函数
    public static student[] arrStudent(student[] arr,int id) {
        //分为两种情况 原数组没满可以继续添加或原数组满了，需要创建一个arr.length+1的一个新数组
        //判断两种情况
        Scanner sc = new Scanner(System.in);
            boolean flag = judgeArrcCpacity(arr);
            if (flag) {
                //创建一个新的数组
                student[] newArr = new student[arr.length + 1];
                //将旧的数组数据传入新数组中
                for (int i = 0; i < arr.length; i++) {
                    newArr[i] = arr[i];
                }
                newArr[arr.length] = stu4;
                //输入要删除的id
                System.out.println("请输入要删除的id：");
                int index = sc.nextInt();
                //通过id删除信息
                deleateID(newArr,index);
                //通过id 使年龄加1岁
                System.out.println("请输入要增加年龄的id：");
                int index1 = sc.nextInt();
                searchID(newArr,index1);
                //打印数组
                print(newArr);
                return newArr;
            } else {
                arr[arr.length - 1] = stu4;
                //输入要删除的id
                System.out.println("请输入要删除的id：");
                int index = sc.nextInt();
                //通过id删除信息
                deleateID(arr,index);
                //通过id 使年龄加1岁
                System.out.println("请输入要增加年龄的id：");
                int index1 = sc.nextInt();
                searchID(arr,index1);
                //打印数组
                print(arr);
                return arr;
            }
        }
    //判断id的唯一性
    public static boolean judgeArrOnly(student[] arr,int id){
            for (int i = 0; i < arr.length; i++) {
                student stu = new student();
                stu = arr[i];
                if(stu != null)
                {
                    if (stu.getId() == id) {
                        //表示已经存在
                        return false;
                    }
                }
            }
            return true;//表示唯一
    }

    //判断数组是否满
    public static boolean judgeArrcCpacity(student[] arr){
        int count = 0;
        for (int i = 0; i < arr.length ; i++) {
            if(arr[i]!=null)
            {
                count++;//count表示需要添加得数的索引
            }
            if(count == arr.length)
            {
                return true;//表示满了
            }
        }
        return false;
        //表示没满
    }

    //遍历数组
    public static void print(student[] arr) {

            for (int i = 0; i < arr.length; i++) {
                student stu = new student();
                stu = arr[i];
                if(stu != null) {
                    System.out.println(stu.getId() + ", " + stu.getName() + ", " + stu.getAge());
                }
            }
        }
        //通过id删除学生信息
    public static student[] deleateID(student[] arr,int id){
        for (int i = 0; i < arr.length; i++) {
            student stu = new student();
            stu = arr[i];
            if(id== stu.getId()){
                arr[i] = null;
                //删除并返回删除后的数组
                return arr;
            }
        }
        System.out.println("删除失败");
        return arr;
    }

    //查找id 增加年龄
    public static student[] searchID(student[] arr,int id){
        for (int i = 0; i < arr.length; i++) {
            //新建一个student
            student stu = new student();
            stu = arr[i];
            if(stu.getId()==id)
            {
                int temp = stu.getAge();
                stu.setAge(temp+1);
                return arr;
            }
        }
        System.out.println("不存在，请重新输入");
        return arr;
    }
}
