package test1;

public class ProjectTest {
    public static void main(String[] args){
        //创建一个数组
        Project[] arr = new Project[3];
        //给三个货物初始值
        Project p1 = new Project("001","iphone",9999,100);
        Project p2 = new Project("002","圣杯",169,109);
        Project p3 = new Project("003","可乐",3,1000);
        //给数组赋值
        arr[0] = p1;
        arr[1] = p2;
        arr[2] = p3;

        //将信息遍历
        for (int i = 0; i < arr.length; i++) {
            // 创建中间值 去调用Project
            Project project = arr[i];
            System.out.println(project.getGoodsID()+","+project.getGoodsName()+","+project.getGoodsPerice()+
                    ","+project.getGoodsRemain());
        }
    }
}
