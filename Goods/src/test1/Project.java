package test1;

public class Project {
    private String goodsID;
    private String goodsName;
    private double goodsPerice;
    private int goodsRemain;

    //构造全参
    public Project(String goodsID,String goodsName,double goodsPerice,int goodsRemain){
        this.goodsID = goodsID;
        this.goodsName = goodsName;
        this.goodsPerice = goodsPerice;
        this.goodsRemain = goodsRemain;
    }

    //构造无参
    public Project(){

    }

    public void setGoodsID(String goodsID){
        this.goodsID = goodsID;
    }
    public String getGoodsID(){
        return goodsID;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public double getGoodsPerice() {
        return goodsPerice;
    }

    public void setGoodsPerice(double goodsPerice) {
        this.goodsPerice = goodsPerice;
    }

    public int getGoodsRemain() {
        return goodsRemain;
    }

    public void setGoodsRemain(int goodsRemain) {
        this.goodsRemain = goodsRemain;
    }

}
