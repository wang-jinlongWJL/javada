package test2;

import java.util.Scanner;

public class CarTest {
    public static void main(String[] args){
        //创建存放 car数据的数组
        Car[] arr = new Car[3];
        Scanner sc = new Scanner(System.in);
        //从键盘录入汽车的数据
        for (int i = 0; i < arr.length; i++) {
            Car car = new Car();//这个应该放在循环里，放在循环外只会重复修改一辆车，应该再循环里创建三辆车
            System.out.println("请输入一个汽车品牌");
            String brand = sc.next();
            car.setBrand(brand);
            System.out.println("请输入汽车的价格");
            int price = sc.nextInt();
            car.setPrice(price);
            System.out.println("请输入汽车的颜色");
            String color = sc.next();
            car.setColor(color);

            //讲这些数据存放到创建的数组中
            arr[i] = car;
        }
        //遍历数组
        for (int i = 0; i < arr.length; i++) {
            Car cars = arr[i];
            System.out.println(cars.getBrand()+","+cars.getPrice()+","+cars.getColor());
        }
    }
}
