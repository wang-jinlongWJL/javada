package test3;

import java.util.Scanner;

public class iphoneTest {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        //创建一个存放手机信息的数组
        iphone[] arr = new iphone[3];
        //从键盘录入数据
        for (int i = 0; i < arr.length; i++) {
            iphone ip = new iphone();
            System.out.println("输入手机的品牌");
            String brand = sc.next();

            ip.setBrand(brand);
            System.out.println("请输入手机的价格");
            int price = sc.nextInt();
            ip.setPrice(price);

            System.out.println("请输入手机的颜色");
            String color = sc.next();
            ip.setColor(color);

            //将录入信息传到数组中
            arr[i] = ip;
        }

        double sum = 0;

        //将信息打印出来并求三部手机的平均值
        for (int i = 0; i < arr.length; i++) {
            iphone ip1 = arr[i];
            System.out.println(ip1.getBrand()+","+ip1.getPrice() +","+ip1.getColor());
            //计算三部手机平均价格
            sum+= ip1.getPrice();
        }
        double average = sum/3.0;
        System.out.printf("三部手机的平均价格为：%.2f",average);
    }
}
