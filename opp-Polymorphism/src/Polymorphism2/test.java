package Polymorphism2;

public class test {
    public static void main(String[] args) {
        animal a = new Dog();
        a.show();
        //编译看左，运行看右。
        //如果父类不能满足子类的需求，需要转换类型
       /* Dog d = (Dog) a;
        d.lookHome();*/
        //简便的运算符
        if(a instanceof Dog){
            Dog d = (Dog) a;
            d.lookHome();
        }
        else if(a instanceof Cat){
            Cat c = (Cat) a;
            c.catchMouse();
        }
    }
}
