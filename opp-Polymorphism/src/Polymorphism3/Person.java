package Polymorphism3;

public class Person {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

//喂养猫和狗的行为直接合并即可
    public void keepPet(Animal a,String something){
        if(a instanceof Dog)
        {
            Dog d = (Dog) a;
            System.out.println("年龄为"+this.age+"的"+this.name+"养了一只"+d.getAge()+"的"+d.getColor()+"的狗");
            d.eat("骨头");
        }
        else if(a instanceof Cat)
        {
            Cat c = (Cat) a;
            System.out.println("年龄为"+this.age+"的"+this.name+"养了一只"+c.getAge()+"的"+c.getColor()+"的猫");
            c.eat("鱼");
        }
    }
}
