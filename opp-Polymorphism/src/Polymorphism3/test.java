package Polymorphism3;

public class test {
    public static void main(String[] args) {
        Person p1 = new Person("老王",25);
        Dog d = new Dog(3,"灰色");
        p1.keepPet(d,"骨头");

        System.out.println("===========================");

        Person p2 = new Person("老李",30);
        Cat c = new Cat(3,"蓝色");
        p2.keepPet(c,"鱼");
    }
}
