package Polymorphism1;

public class test {
    public static void main(String[] args) {
        Student sd = new Student();
        sd.setName("王五");
        sd.setAge(18);

        Teacher tc = new Teacher();
        tc.setName("赵六");
        tc.setAge(44);

        administrator admin = new administrator();
        admin.setName("李四");
        admin.setAge(26);

        regester(sd);
        regester(tc);
        regester(admin);


    }
    //写一个注册方法，只需要写一个父类的参数
    public static void regester(Person p){
        p.show();
    }
}
